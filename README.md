# Družbeno omrežje za vse ljubitelje takšnih in drugačnih elektronskih naprav

Seminar po predlogi Gadget Freak

## Problem

Vsak dan na trg prihajajo nove naprave, javnost pa težko sledi vsemu temu napredku. Spletna stran želi olajšati uporabnikom seznaniti se z novostmi na trgu računalniške, telefonske in ostale elektrotehnike z opisi opreme, objavljenimi testi ter z izmenjavo izkušenj preko integriranega foruma. 


## Funkcionalnosti

- registracija novega uporabnika
- dodajanje nove naprave
- dodajanje tehničnih lastnosti naprave
- dodajanje slik naprave
- dodajanje preizkusov naprave
- forum za vsako napravo

# Ciljna publika

- Vsi, ki radi spremljajo novosti
- Potencialni kupci, ki iščejo mnenja
