﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nancyserver {
    public class ArticleModel {
        public string username;
        public string picture;
        public string title;
        public DateTime created;
        public string createdString;
        public int id;
    }

    public class UserModel {
        public string username;
        public string email;
        public string password;
        public string password2;
        public int id;
    }

    public class CommentModel {
        public int id;
        public string username;
        public int user_id;
        public int article_id;
        public string content;
        public DateTime created;
        public string createdString;
    }

}
