﻿using System;
using System.Collections.Generic;
using System.Linq;

using Nancy;
using Nancy.Security;
using Nancy.Bootstrapper;
using Nancy.Authentication.Forms;
using Nancy.TinyIoc;

using System.Dynamic;
using Nancy.Extensions;

using static NancyServer.Helper;
using Dapper;
using Nancy.ModelBinding;
using nancyserver;

namespace NancyServer {

    public class User : IUserIdentity {
        public string UserName { get; set; }
        public IEnumerable<string> Claims { get; set; }
    }

    public class FormsAuthBootstrapper : DefaultNancyBootstrapper {
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines) {
            base.ApplicationStartup(container, pipelines);
            Nancy.Security.Csrf.Enable(pipelines);
        }

        protected override void ConfigureApplicationContainer(TinyIoCContainer container) {
            // We don't call "base" here to prevent auto-discovery of types/dependencies
        }

        protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context) {
            base.ConfigureRequestContainer(container, context);
            container.Register<IUserMapper, UserDatabase>();
        }

        protected override void RequestStartup(TinyIoCContainer requestContainer, IPipelines pipelines, NancyContext context) {
            var formsAuthConfiguration =
                new FormsAuthenticationConfiguration() {
                    RedirectUrl = "~/login",
                    UserMapper = requestContainer.Resolve<IUserMapper>(),
                };

            FormsAuthentication.Enable(pipelines, formsAuthConfiguration);
        }
    }



    public class LoginModule : NancyModule {
        public LoginModule() {
               
            Get["/login"] = x => {
                dynamic model = new ExpandoObject();
                model.Errored = this.Request.Query.error.HasValue;
                return View["login.cshtml", model];
            };

            Post["/login"] = x => {
                var userGuid = UserDatabase.ValidateUser((string)this.Request.Form.Username, (string)this.Request.Form.Password);

                if (userGuid == null) {
                    return this.Context.GetRedirect("~/login?error=true&username=" + (string)this.Request.Form.Username);                   
                }

                DateTime? expiry = null;
                if (this.Request.Form.RememberMe.HasValue) {
                    expiry = DateTime.Now.AddDays(7);
                }

                return this.LoginAndRedirect(userGuid.Value, expiry);
            };

            Post["/register"] = x => {
                var user = this.Bind<UserModel>();
                if (!user.password2.Equals(user.password2)) return "passwords don't match";

                var hash = saltAndHash(user.password, getNewSalt());
                Console.WriteLine(hash);
                var userGuid = Guid.NewGuid();

                sql.Execute("INSERT INTO users (username, email, power_level, salthash, guid) VALUES (@user, @email, 100, @hash, @guid);",
                new { user = user.username, user.email, hash=hash, guid = userGuid.ToString().ToUpper()});

                var expiry = DateTime.Now.AddDays(7);
                return this.LoginAndRedirect(userGuid, expiry);
            };


            Get["/logout"] = x => {
                return this.LogoutAndRedirect("~/");
            };
        }
    }

    
    public class UserDatabase : IUserMapper {
        public IUserIdentity GetUserFromIdentifier(Guid identifier, NancyContext context) {
            var name = sql.Query<String>("SELECT username FROM users WHERE guid = @gstr", new { gstr = identifier.ToString().ToUpper()}).FirstOrDefault();
            return name == null ? null: new User { UserName = name };
        }

        public static Guid? ValidateUser(string username, string password) {
            var r = sql.Query("select salthash, guid FROM users WHERE username = @username", new { username }).FirstOrDefault();
            if (r == null) return null;
            if (!comparePasswordToHash(r.salthash, password)) r = null;
            if(r == null) {
                return null;
            } else {
                return new Guid(r.guid);
            }
        }
    }


}