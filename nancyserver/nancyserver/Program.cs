﻿using System;
using Nancy;
using Nancy.Hosting.Self;
using System.Linq;
using System.Data.SqlClient;

using System.Threading.Tasks;

using Dapper;
using Npgsql;
using static NancyServer.Helper;
using Nancy.Cryptography;
using Nancy.TinyIoc;
using Nancy.Bootstrapper;

namespace NancyServer {
    public class MainClass {
        
		public static void Main(string[] args) {

            new HostConfiguration() {
                UrlReservations = new UrlReservations() { CreateAutomatically = true }
            };
            
            //open connection to database
			sql = new  NpgsqlConnection(System.IO.File.ReadAllText("Misc/connectionstring.txt").Trim());
			sql.Open();

			StaticConfiguration.DisableErrorTraces = false;

			using (var host = new NancyHost(new Uri("http://localhost:8080"))) {
				host.Start();
                if(System.Diagnostics.Debugger.IsAttached) System.Diagnostics.Process.Start("http://localhost:8080/"); 
                Console.ReadLine();
			}

		}
	}	
    
}
