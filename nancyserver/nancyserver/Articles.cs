﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Nancy;
using static NancyServer.Helper;
using Dapper;
using Nancy.ModelBinding;
using Nancy.Security;

namespace nancyserver {


    public class Articles : NancyModule {
        public Articles() {
        
            Get["/articles/{a:int}"] = parameters => {            
                var article = (from r in sql.Query("select username, content from articles INNER JOIN  users ON articles.user_id = users.id WHERE  articles.id = @a", new { a = (int)(parameters.a) })
                               select new { r.username, r.content }.ToExpando()).First();

                var comments = from r in sql.Query("select username, comments.content, comments.created from comments INNER JOIN articles ON comments.article_id = articles.id INNER JOIN users ON comments.user_id = users.id WHERE articles.id = @a ORDER BY comments.created", new { a = (int)(parameters.a) })
                               select new { r.username, r.content, r.created }.ToExpando();
                
                return View["article.cshtml", new { article, comments }.ToExpando()];
            };
    
 
            Get["/articles/"] = parameters => {
                var model = from r in sql.Query<ArticleModel>("SELECT username, title, created, articles.id FROM articles INNER JOIN users on articles.user_id = users.id ORDER BY articles.created;")
                            let d = (DateTime)r.created
                            select new ArticleModel { username = r.username, title = r.title, createdString = timeFromNow(d), id = r.id, picture = "phone.jpg" };

                return View["articles.cshtml", model.Take(6)];
            };

            Post["/articles/{a:int}"] = parameters => {
                this.RequiresAuthentication();
                this.ValidateCsrfToken();

                var comment = this.Bind<CommentModel>();
                
                int article_id = (int)parameters.a;

                var user_id = sql.Query<UserModel>("SELECT id from users WHERE username=@name;", new { name = Context.CurrentUser.UserName }).First().id;

                sql.Execute("INSERT INTO comments (content , created, user_id, article_id) VALUES (@content, @date, @user, @article);",
                    new {content = comment.content, date = DateTime.Now, user=user_id, article=article_id});

                return Response.AsRedirect("/"); 
            };




        }
    }

   
   

}

