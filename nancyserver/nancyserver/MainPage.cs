﻿using Nancy;
using static NancyServer.Helper;
using Dapper;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace nancyserver {
    
    public class MainPage : NancyModule {
        public MainPage() {
            Get["/"] = parameters => {
                var model = from r in sql.Query<ArticleModel>("SELECT username, title, created, articles.id FROM articles INNER JOIN users on articles.user_id = users.id;")
                            let d = (DateTime)r.created
                            select new ArticleModel {username = r.username, title = r.title, createdString = timeFromNow(d), id = r.id, picture = "phone.jpg"};                
            
                return View["index.cshtml", model.Take(6)];
            };

        }
    }

}
