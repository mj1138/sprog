﻿using Nancy;
using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NancyServer {
    static class Helper {
        public static NpgsqlConnection sql;

        public static string timeFromNow(DateTime d) {
            var t = DateTime.Now - d;
            if (t.TotalHours > 24) return d.ToShortDateString();
            else if(t.TotalHours > 1) return t.TotalHours + " s ago";
            else return t.TotalMinutes + " minutes ago";
        }

        //http://stackoverflow.com/questions/5120317/dynamic-anonymous-type-in-razor-causes-runtimebinderexception/5670899#5670899
        public static ExpandoObject ToExpando(this object anonymousObject) {
            IDictionary<string, object> expando = new ExpandoObject();
            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(anonymousObject)) {
                var obj = propertyDescriptor.GetValue(anonymousObject);
                expando.Add(propertyDescriptor.Name, obj);
            }
            return (ExpandoObject)expando;
        }


        public static string saltAndHash(string password, string salt) {
            var pb = Encoding.UTF8.GetBytes(password);
            var sb = Encoding.UTF8.GetBytes(salt);
            var sha256 = new SHA256Managed().ComputeHash(pb.Concat(sb).ToArray());
            return salt+":"+Convert.ToBase64String(sha256);
        }

        public static string getNewSalt() {
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider()) {
                byte[] rnd = new byte[20];
                rng.GetBytes(rnd);
                return Convert.ToBase64String(rnd);
            }
        }

        public static bool comparePasswordToHash(string hash, string password) {
            var split = hash.Split(':');
            return saltAndHash(password, split[0]).Equals(hash);
        }

    }



}
